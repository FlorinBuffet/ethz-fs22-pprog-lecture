package L02_01_ByteCodeInc;


public class Increment {

    public static void main(String[] args) {

        //int i = 1 + 2;
        int a = 1;
        int b = 2;
        int i = a + b;

        System.out.println("i: " + i);
    }

}
