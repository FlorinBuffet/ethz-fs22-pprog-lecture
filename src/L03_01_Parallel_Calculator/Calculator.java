package L03_01_Parallel_Calculator;

import java.util.Random;

public class Calculator implements Runnable {
    private int number;

    public Calculator(int number) {
        this.number = number;
    }

    @Override
    public void run() {
        Random random = new Random();

        for (int i = 1; i <= 10; i++) {
            // Print Thread-number, Multiplication and Product
            System.out.printf("%s:%d * %d = %d\n", Thread.currentThread().getName(), number, i, number * i);

            // Wait some Milliseconds to switch to another Thread
            // If nextInt has a 1, there is almost no time, so the threads execute multiple
            // loops, if it's 100 it switches after each loop
            try {
                Thread.sleep(random.nextInt(1));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
